const icons = {
  tabBar: {
    home: require("./tabBar/home/home.png"),
    camera: require("./tabBar/camera/camera.png"),
    explore: require("./tabBar/explore/explore.png"),
    friends: require("./tabBar/friends/friends.png"),
    payment: require("./tabBar/payment/payment.png"),
    favorite: require("./tabBar/favorite/favorite.png"),
    map: require("./tabBar/map/map.png")
  },
  infoCard: {
    pin: require("./infoCard/pin/pin.png")
  },
  back: require("./back/back.png"),
  backMenu: require("./backMenu/backMenu.png"),
  bankStatement: require("./bankStatement/bankStatement.png"),
  camera: require("./camera/camera.png"),
  card: require("./card/card.png"),
  cashback: require("./cashback/cashback.png"),
  close: require("./close/close.png"),
  filter: require("./filter/filter.png"),
  flashOn: require("./flashOn/flashOn.png"),
  flashOff: require("./flashOff/flashOff.png"),
  insertText: require("./insertText/insertText.png"),
  like: require("./like/like.png"),
  notification: require("./notification/notification.png"),
  pin: require("./pin/pin.png"),
  profile: require("./profile/profile.png"),
  profileMenu: require("./profileMenu/profileMenu.png"),
  question: require("./question/question.png"),
  reverseCamera: require("./reverseCamera/reverseCamera.png"),
  search: require("./search/search.png"),
  shutterButton: require("./shutterButton/shutterButton.png"),
  textColorNormal: require("./textColorNormal/textColorNormal.png"),
  textColorInverted: require("./textColorInverted/textColorInverted.png")
};

export default icons;
