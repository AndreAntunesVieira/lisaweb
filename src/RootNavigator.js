import React, { Fragment } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Welcome from "./pageComponents/Welcome";
import Home from "./pageComponents/Home";
import Name from "./pageComponents/Name";
import Quiz from "./pageComponents/Quiz";
import Result from "./pageComponents/Result";


//The component Router is the manager of all routes (which component will render for each url address)
//The Fragment is needed because Router just accept one component directly as child
export default () => (
  <Router>
    <Fragment>
      <Route exact path="/" component={Welcome} />
      <Route exact path="/Name" component={Name} />
      <Route exact path="/Home" component={Home} />
      <Route exact path="/Play" component={Quiz} />
      <Route exact path="/Result" component={Result} />
    </Fragment>
  </Router>
);
