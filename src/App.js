import "redux";
import React from "react";
import { Provider } from "react-redux";
import store from "./redux/Store";
import RootNavigator from "./RootNavigator";

//This is the way to add redux to react, the real application is the RootNavigator
const App = () => (
  <Provider store={store}>
    <RootNavigator />
  </Provider>
);

export default App;
