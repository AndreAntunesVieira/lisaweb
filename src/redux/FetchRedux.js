import { asyncTypesFactory } from '../helpers/ActionsHelpers'

//TYPES
const featchQuizTypes = asyncTypesFactory("quiz/FETCH_QUIZ");
export const [FETCH_QUIZ, FETCH_QUIZ_SUCCESS, FETCH_QUIZ_FAIL] = featchQuizTypes;

//REDUCERS
const INITIAL_STATE = {
  questions: [ ],
  numberOfQuestions: 0,
}

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_QUIZ:
      return { ...INITIAL_STATE }
    case FETCH_QUIZ_SUCCESS:
      return {
        ...INITIAL_STATE,
        ...state,
        questions: action.payload,
        numberOfQuestions: action.payload.length,
      }
    case FETCH_QUIZ_FAIL:
      return {
        ...INITIAL_STATE,
      }
    default:
      return state
  }
}
