import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk";
import { combineReducers } from "redux";

import fetchQuestion from "./FetchRedux";
import user from "./UserRedux";
import answer, { RESET_TO_PLAY_AGAIN } from "./AnswersRedux";
import status from "./StatusRedux";

function getCompose() {
  if (typeof window === "undefined") return compose;
  return window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const appReducer = combineReducers({ fetchQuestion, answer, user, status });

const rootReducer = (state, action) => {
  if (action.type === RESET_TO_PLAY_AGAIN) state = undefined;
  return appReducer(state, action);
};

export default createStore(rootReducer, {}, getCompose()(applyMiddleware(ReduxThunk)));
