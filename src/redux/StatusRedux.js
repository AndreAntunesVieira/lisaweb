import { getQuestions } from "../helpers/Api";
import { decodeQuestions } from "../helpers/Questions";
import { FETCH_QUIZ, FETCH_QUIZ_FAIL, FETCH_QUIZ_SUCCESS } from './FetchRedux'


//ACTIONS
export const fetchQuestions = () => async dispatch => {
  dispatch({ type: FETCH_QUIZ });
  const promiseQuestions = await getQuestions();
  if (promiseQuestions.status === "success") {
    dispatch({ type: FETCH_QUIZ_SUCCESS, payload: decodeQuestions(promiseQuestions.data) });
  } else {
    dispatch({ type: FETCH_QUIZ_FAIL, payload: promiseQuestions.errorMessage });
  }
};


//REDUCER
const INITIAL_STATE = {
  hasError: false,
  errorMessage: undefined,
  isLoading: true,
}

export default function statusRedux(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_QUIZ:
      return { ...INITIAL_STATE }

    case FETCH_QUIZ_SUCCESS:
      return {
        ...state,
        isLoading: false
      }

    case FETCH_QUIZ_FAIL:
      return {
        ...INITIAL_STATE,
        hasError: true,
        errorMessage: action.payload,
        isLoading: false
      }

    default:
      return state
  }
}
