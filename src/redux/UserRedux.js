//ACTIONS
export const saveUserTime = time => ({ type: "SAVE_USER_TIME", time });
export const setUserName = name => ({ type: "SET_USER_NAME", name });

//REDUCER
const INITIAL_STATE = {};
export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "SET_USER_NAME": {
      return {
        ...state,
        name: action.name,
      };
    }

    case "SAVE_USER_TIME": {
      return {
        ...state,
        time: action.time,
      };
    }

    default:
      return state;
  }
}
