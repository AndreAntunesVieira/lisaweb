//TYPES
export const ON_ANSWER = 'answer/ON_ANSWER'
export const RESET_TO_PLAY_AGAIN = 'answer/RESET_TO_PLAY_AGAIN'

//ACTIONS
export const resetToPlayAgain = () => ({ type: RESET_TO_PLAY_AGAIN });
export const onAnswer = answer => {
  const { question, correct_answer } = answer.question;
  const isCorrect = correct_answer === answer.chosen_option;
  return { type: ON_ANSWER, question, isCorrect };
};


//REDUCERS
const INITIAL_STATE = {
  counter: 0,
  results: [],
  score: 0,
};

export default function answersRedux(state = INITIAL_STATE, { type, ...action }) {
  switch (type) {
    case ON_ANSWER:
      return {
        ...state,
        counter: state.counter + 1,
        results: [...state.results, action],
        score: action.isCorrect ? state.score + 1 : state.score,
      };
    default:
      return state;
  }
}
