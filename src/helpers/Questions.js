export function decodeQuestions(array) {
  return array.map(item => ({ ...item, question: item.question }));
}
