import { QuestionsApi, snapShootToArray } from "./DatabaseHelpers";

export async function getQuestions() {
  const snapShoot = await QuestionsApi.once("value");
  return {
    status: "success",
    data: snapShootToArray(snapShoot),
  };
}
