import firebase from 'firebase'
import firebaseConfig from '../.firebaseCredentials'

firebase.initializeApp(firebaseConfig);

const Database = firebase.database()
export const QuestionsApi = Database.ref('questions')
export const HighscoreApi = Database.ref('highscore')

// Firebase returns a specific firebase object, this method transform to a list (array)
export function snapShootToArray(snapShoot){
  const arr = []
  snapShoot.forEach(row => {
    arr.push(fixSnapShootArrays(row))
  })
  return arr
}

// Fix each element of the snapshoot array if this element have a wrong object/array problems
function fixSnapShootArrays(snapShoot){
  const json = snapShoot.toJSON()
  Object.entries(json).forEach(([name,value]) => {
    if(isAFakeArray(value)) json[name] = Object.values(value)
  })
  return json
}

// Firebase snapShoot to Json returns a wrong array like {0: 'Something', 1: 'Other thing'} and not ['Something', 'Other Thing'],
// this method verify if this each element is one of this wrong arrays
function isAFakeArray(obj){
  if(typeof obj !== 'object') return false
  const keys = Object.keys(obj)
  return keys.every(key => !Number.isNaN(key))
}

