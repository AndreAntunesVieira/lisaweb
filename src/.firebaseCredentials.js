const useAndreFirebase = true

const andreFirebaseConfig = {
  apiKey: "AIzaSyDl7U--_dm6md4aKvexKfz_sG9L0jL1w90",
  authDomain: "my-database-64a29.firebaseapp.com",
  databaseURL: "https://my-database-64a29.firebaseio.com",
  projectId: "my-database-64a29",
  storageBucket: "my-database-64a29.appspot.com",
  messagingSenderId: "157374624897"
};

const lisaFirebaseConfig = {
  apiKey: "AIzaSyDvW8seq9Nyk-8ct6MBjYg7Ka_Avnp8erE",
  authDomain: "my-database-c0eea.firebaseapp.com",
  databaseURL: "https://my-database-c0eea.firebaseio.com",
  projectId: "my-database-c0eea",
  storageBucket: "my-database-c0eea.appspot.com",
  messagingSenderId: "242022263498"
};

export default useAndreFirebase ? andreFirebaseConfig : lisaFirebaseConfig
