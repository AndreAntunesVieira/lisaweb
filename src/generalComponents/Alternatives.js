import React from "react";
import { randomizeArray } from '../helpers/ArrayManipulation'

const Alternatives = ({ onClick, correctAnswer, incorrectAnswers }) => {
  const allAlternatives = [correctAnswer, ...incorrectAnswers];
  const randomlySortedAlternatives = randomizeArray(allAlternatives);
  return (
    <section>
      {randomlySortedAlternatives.map(label => (
        <button className="btn btn-primary btn-full margin-bottom-small" key={label} onClick={() => onClick(label)}>
          {label}
        </button>
      ))}
    </section>
  );
};

export default Alternatives;
