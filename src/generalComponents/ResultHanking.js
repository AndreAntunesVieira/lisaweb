import React, { Component } from "react";
import { HighscoreApi } from "../helpers/DatabaseHelpers";

class ResultRanking extends Component {
  state = { entries: [] };

  componentWillMount() {
    HighscoreApi.orderByChild("orderer")
      .limitToLast(10)
      .once("value")
      .then(snapShoot => {
        const entries = [];
        snapShoot.forEach(el => {
          entries.push(el.toJSON());
        });
        this.setState({ entries });
      });
  }

  render() {
    return (
      <div>
        <h1 className="text-center">High Score Rankings</h1>
        <table className="table table-dark">
          <tr>
            <th style={{ width: 60 }}>Name</th>
            <th style={{ width: 50 }}>Score</th>
            <th style={{ width: 100 }}>Time</th>
          </tr>
          {this.state.entries.reverse().map(entry => (
            <tr>
              <td>{entry.name}</td>
              <td>{entry.score}</td>
              <td>{`${entry.time / 1000}s`}</td>
            </tr>
          ))}
        </table>
      </div>
    );
  }
}
export default ResultRanking;
