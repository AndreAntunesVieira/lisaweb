import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";

//This is the way to "inject" React on public/index.html... using the element with id "root"
ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
