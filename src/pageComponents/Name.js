import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { setUserName } from '../redux/UserRedux'

class Name extends Component {
  onChangeName = event => {
    //All single key pressed call this method (onChangeName)
    if (event.target.value.length <= 3) {
      //if have less than 4 letters on input enter here
      //This method calls a redux action "SET_USER_NAME" passing the actual value of the input
      this.props.setUserName(event.target.value);
      //This return just stop the method "onChangeName"... the lines below will not run
      return null;
    }

    //Force input to have only 3 letters
    event.target.value = event.target.value.substr(0, 3);
  };

  onButtonClick = () => {
    //Here you can receive the "event" as parameter but if is not used you can just not declare "onButtonClick = event =>"
    this.props.history.push("Home");
  };

  render = () => {
    return (
      <section className="container main-container">
        <h1 className="margin-bottom-large text-strong">Before you start please enter your 3 initials</h1>
        <input className="form-control name-input margin-bottom-small" onChange={this.onChangeName} />
        <small className="text-center margin-bottom-medium">
          (initial of first name, initial of middle name, initial of last name).Thank you!
        </small>
        <button className="btn btn-primary" onClick={this.onButtonClick}>
          NEXT
        </button>
      </section>
    );
  };
}

const mapStateToProps = null
const mapActionsToProps = { setUserName }

// The line below connects:
// - Name component to redux: connect(mapStateToProps, mapActionsToProps)(Name)
//   - map no state to props (null), so no one variable from store will be added to component Name
//   - map the action setUserName to Name component, this allow you to use this.props.setUserName
//
// Connect the router (withRouter), so you can use this.props.history
export default withRouter(connect(mapStateToProps, mapActionsToProps)(Name));
