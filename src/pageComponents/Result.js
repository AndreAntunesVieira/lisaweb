import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { HighscoreApi } from "../helpers/DatabaseHelpers";
import ResultHanking from "../generalComponents/ResultHanking";
import { resetToPlayAgain } from "../redux/AnswersRedux";

class Result extends Component {
  componentWillMount() {
    if (!this.props.score || !this.props.name || !this.props.time) {
      return this.props.history.push("/Name");
    }
    HighscoreApi.push({
      score: this.props.score,
      name: this.props.name,
      time: this.props.time,
      orderer: `${this.props.score.toString().padStart(2, "0")}_${9999999999999 - this.props.time}`,
    });
  }

  handlePlayAgain = () => {
    const { history, resetToPlayAgain } = this.props;
    resetToPlayAgain();
    history.push("/Name");
  };

  render() {
    const { results, score, numberOfQuestions, isLoading } = this.props;

    if (!results || (results.length === 0 && !isLoading)) {
      throw new Error("Unable to show results");
    }

    return (
      <div className="main-container container">
        <div className="font-size-20 text-strong text-center">
          You scored {score} / {numberOfQuestions}
        </div>

        <ResultHanking />
        <div>
          {results.map(item => (
            <div key={item.question}>
              {item.isCorrect ? <img className="icon" src="/correct.png" /> : <img className="icon" src="/wrong.png" />}
              {item.question}
            </div>
          ))}
        </div>

        <button className="btn btn-primary" onClick={this.handlePlayAgain}>
          PLAY AGAIN
        </button>
      </div>
    );
  }
}

const mapStateToProps = ({
  answer: { score, results },
  fetchQuestion: { numberOfQuestions },
  status: { isLoading },
  user: { name, time },
}) => {
  return { results, score, numberOfQuestions, isLoading, name, time };
};

const connectedResultScreen = connect(
  mapStateToProps,
  { resetToPlayAgain }
)(Result);

export default withRouter(connectedResultScreen);
