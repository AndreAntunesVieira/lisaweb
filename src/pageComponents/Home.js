import React from "react";
import { Route, withRouter } from "react-router-dom";

// This is a simple component so can use an simple arrow function receiving props as parameters
const Home = props => (
  <section className="container main-container">
    <h1 className="margin-bottom-medium">Welcome to the Trivia Challenge!</h1>
    <img src="/welcome.png" className="margin-bottom-medium" />
    <div className="text-center font-size-30 margin-bottom-medium">
      You will be presented with 10 Multiple Choice questions <br />
      Can you score 100%?
    </div>
    <button className="btn btn-primary" onClick={() => props.history.push("Play")}>
      Begin
    </button>
  </section>
);

//Note that you call the route "Play" and not the component "Quiz", look the route again: (src/RootNavigation.js)
// <Route exact path="/Play" component={Quiz} />

export default withRouter(Home);
