import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Alternatives from "../generalComponents/Alternatives";
import { onAnswer } from "../redux/AnswersRedux";
import { saveUserTime } from "../redux/UserRedux";
import { fetchQuestions } from "../redux/StatusRedux";

class Quiz extends Component {
  componentDidMount() {
    //Just when start the quiz, save the current time
    this.startTimer = new Date().getTime();
    //Dispatch a action that will get al questions from firebase
    this.props.fetchQuestions();
  }

  componentWillReceiveProps(nextProps) {
    //Each time that props change will enter here
    //when respond each question for example...
    const { numberOfQuestions, counter, isLoading, hasError } = nextProps;
    const isLastQuestion = counter === numberOfQuestions && !isLoading && !hasError;

    if (isLastQuestion) {
      //After all questions get the currentTime
      const end = new Date().getTime();
      //Save the difference of time
      this.props.saveUserTime(end - this.startTimer);
      this.props.history.push("Result");
    }
  }

  handleAnswer = value => {
    const { questions, counter } = this.props;
    const { question, correct_answer } = questions[counter];
    this.props.onAnswer({ question: { question, correct_answer }, chosen_option: value });
  };

  renderLoadingState = () => {
    return (
      <section className="main-container">
        <img src="/loading.png" />
      </section>
    );
  };

  render() {
    const { isLoading, hasError, errorMessage, questions, numberOfQuestions, counter } = this.props;

    if (hasError) throw new Error(errorMessage);

    if (isLoading || !questions[counter]) return this.renderLoadingState();

    const { question, correct_answer, incorrect_answers } = questions[counter];
    const currentQuestion = counter + 1;
    return (
      <section className="main-container">
        <div className="card margin-bottom-small">{question}</div>
        <div className="text-center text-center font-size-20 margin-bottom-small">{`${currentQuestion} of ${numberOfQuestions}`}</div>
        <Alternatives onClick={this.handleAnswer} correctAnswer={correct_answer} incorrectAnswers={incorrect_answers} />
      </section>
    );
  }
}

const mapStateToProps = ({
  fetchQuestion: { questions, numberOfQuestions },
  answer: { counter },
  status: { hasError, errorMessage, isLoading },
}) => {
  return { questions, numberOfQuestions, isLoading, counter, hasError, errorMessage };
};

const mapActionsToProps = { fetchQuestions, onAnswer, saveUserTime };

const connectedToReduxQuizScreen = connect(
  mapStateToProps,
  mapActionsToProps
)(Quiz);

export default withRouter(connectedToReduxQuizScreen);
