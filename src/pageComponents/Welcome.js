import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class Welcome extends Component {
  componentDidMount() {
    //Wait 1.5 seconds and redirect automaticaly to Name page
    setTimeout(() => this.props.history.push("Name"), 1500);
  }

  render() {
    return (
      <section className="main-container container">
        <img src="/loading.png" />
        <h1>Loading...</h1>
      </section>
    );
  }
}
//This component is not connected to redux so dont need a mapStateToProps or mapActionsToProps
//This component is connected with the router, this add to Welcome component the props "history"
export default withRouter(Welcome);
